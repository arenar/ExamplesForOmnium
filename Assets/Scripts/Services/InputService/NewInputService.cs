using UnityEngine;

namespace TaxiManiac.InputSystem
{
    public class NewInputService : InputService
    {
        private InputController _inputController;


        public override Vector2 Direction =>
            InputController.CarControl.Direction.ReadValue<Vector2>();

        private InputController InputController
        {
            get
            {
                Initialize();
                return _inputController;
            }
        }
        
        
        public override void Initialize()
        {
            if (_inputController != null)
                return;
            
            _inputController = new InputController();
            _inputController.CarControl.Enable();
        }
    }
}