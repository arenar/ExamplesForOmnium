using UnityEngine;

namespace TaxiManiac.InputSystem
{
    public interface IInputService
    {
        Vector2 Direction { get; }


        void Initialize();
    }
}