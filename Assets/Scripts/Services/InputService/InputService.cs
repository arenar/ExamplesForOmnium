using UnityEngine;

namespace TaxiManiac.InputSystem
{
    public abstract class InputService : MonoBehaviour, IInputService
    {
        public abstract Vector2 Direction { get; }


        public abstract void Initialize();
    }
}