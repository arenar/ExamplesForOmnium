using UnityEngine;

namespace TaxiManiac.InputSystem
{
    public class OldInputService : InputService
    {
        public override Vector2 Direction =>
            new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));


        public override void Initialize()
        {
            return;
        }
    }
}