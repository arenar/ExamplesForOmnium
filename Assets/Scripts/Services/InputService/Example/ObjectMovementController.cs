using UnityEngine;

namespace TaxiManiac.InputSystem.Example
{
    public class ObjectMovementController : MonoBehaviour
    {
        [SerializeField] private InputService _inputService;
        [SerializeField] private float _force = 5.0f;

        private Rigidbody _objectRb;


        private void Start()
        {
            _objectRb ??= gameObject.GetComponent<Rigidbody>();
        }
        
        private void Update()
        {
            if (_inputService.Direction == Vector2.zero)
                return;

            
            _objectRb.AddForce(new Vector3(_inputService.Direction.x, 0, _inputService.Direction.y)
                               * _force * Time.deltaTime, ForceMode.Impulse);
        }
    }
}