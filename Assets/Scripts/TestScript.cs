using TaxiManiac.AudioSystem;
using UnityEngine;


public class TestScript : MonoBehaviour
{
    [SerializeField] private AudioSource _audioSource;
    [SerializeField] private SoundType _soundType;
    
    private AudioController _audioController;


    private SimpleAudioSystemService SimpleAudioSystemService =>
        (SimpleAudioSystemService)AudioSystemService.Instance;


    public void PlaySound()
    {
        _audioSource ??= SimpleAudioSystemService.CreateAudioSource(this.gameObject, AudioSystemType.Sounds);
        _audioController ??= new AudioController(_audioSource);

        var sound = SimpleAudioSystemService.SoundsDictionary[_soundType];
        _audioController.PlaySound(sound);
    }

    public void SetAudioEnable()
    {
        SimpleAudioSystemService.SetVolume(AudioSystemType.Sounds, true);
    }

    public void SetAudioDisable()
    {
        SimpleAudioSystemService.SetVolume(AudioSystemType.Sounds, false);
    }
}
